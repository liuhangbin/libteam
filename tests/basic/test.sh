libteam_setup_files()
{
	cat <<-EOF > activebackup.json
	{
	        "device":"team0",
	        "runner":{"name":"activebackup"},
	        "link_watch": {"name": "ethtool"}
	}
	EOF

	cat <<-EOF >lacp.json
	{
	    "device":"team0",
	    "runner":{
	                "name":"lacp",
	                "active": false,
	                "fast_rate": true,
	                "tx_hash": ["eth", "ipv4", "ipv6"]},
	    "link_watch": {"name": "ethtool"}
	}
	EOF

	cat <<-EOF > roundrobin.json
	{
	    "device":"team0",
	    "runner":{
	                "name":"roundrobin",
	                "fast_rate": true
	              },
	    "link_watch": {"name": "ethtool"}
	}
	EOF

	cat <<-EOF > broadcast.json
	{
	        "device":"team0",
	        "runner":{"name":"broadcast"},
	        "link_watch": {"name": "ethtool"}
	}
	EOF

	cat <<-EOF > loadbalance.json
	{
	    "device":"team0",
	    "runner":{
	                "name":"loadbalance",
	                "tx_balancer" : { "name" : "basic", "balancing_interval" : 10 },
	                "tx_hash": ["eth", "ipv4", "ipv6"] },
	    "link_watch": {"name": "ethtool"}
	}
	EOF
}

libteam_clean_files()
{
	rm -rf *.json
}

libteam_log()
{
	echo -e "[libteam gating]: $@"
	echo -e "[libteam gating]: $@" >> libteam.log
}

libteam_start_ovs()
{
	local url=http://download-node-02.eng.bos.redhat.com/brewroot/packages

	ver=`curl -s $url/openvswitch2.15/2.15.0/ | grep -o "[0-9]*.el9fdp" |tail -1`
	pver=`curl -s $url/openvswitch-selinux-extra-policy/1.0/ |grep -o "[0-9]*.el9fdp" |tail -1`
	yum install -y $url/openvswitch2.15/2.15.0/$ver/x86_64/openvswitch2.15-2.15.0-$ver.x86_64.rpm \
	$url/openvswitch-selinux-extra-policy/1.0/$pver/noarch/openvswitch-selinux-extra-policy-1.0-$pver.noarch.rpm

	systemctl start openvswitch
}

libteam_stop_ovs()
{
	systemctl stop openvswitch
}

libteam_setup_switch()
{
	[ $runner = "lacp" ] || return

	ovs-vsctl del-port br0 br_ceth1
	ovs-vsctl del-port br0 br_ceth2
	ovs-vsctl add-bond br0 br_lacp1 br_ceth1 br_ceth2 lacp=active

	sleep 3
}

libteam_clean_switch()
{
	[ $runner = "lacp" ] || return

	ovs-vsctl del-port br0 br_lacp1
	ovs-vsctl add-port br0 br_ceth1
	ovs-vsctl add-port br0 br_ceth2
}

libteam_setup()
{
	# ceth1 --- br_ceth1 \
	#                     br0 - br_seth1 --- seth1 (server)
	# ceth2 --- br_ceth2 /

	# do topo setup
	ip netn add serv1
	ip link add ceth1 type veth peer name br_ceth1
	ip link add ceth2 type veth peer name br_ceth2
	ip link add seth1 type veth peer name br_seth1
	ip link set seth1 netns serv1

	libteam_start_ovs
	ovs-vsctl add-br br0
	ovs-vsctl add-port br0 br_ceth1
	ovs-vsctl add-port br0 br_ceth2
	ovs-vsctl add-port br0 br_seth1
	ip link set br0 up
	ip link set br_ceth1 up
	ip link set br_ceth2 up
	ip link set br_seth1 up

	ip netns exec serv1 ip link set seth1 up
	ip netns exec serv1 ip addr add 192.168.11.1/24 dev seth1

	libteam_setup_files
	rm -rf libteam.log
	libteam_log "testing setup\n\n"
}

libteam_test_nm()
{
	local runner=$1

	nmcli con add con-name team0 type team ifname team0 ipv4.method manual ipv4.addr 192.168.11.2/24 connection.autoconnect no
	nmcli con modify team0 team.config $runner.json
	nmcli con add type ethernet con-name ceth1 ifname ceth1 master team0
	nmcli con add type ethernet con-name ceth2 ifname ceth2 master team0

	nmcli con reload
	nmcli con up team0
	nmcli con up ceth1
	nmcli con up ceth2

	timeout 60s bash -c "until ping -c 3 192.168.11.1 >> libteam.log; do sleep 5; done" || \
	{ let LIBTEAM_FAIL++; libteam_log "FAIL $LIBTEAM_FAIL: nm $runner testing failed"; }

	nmcli con delete ceth1
	nmcli con delete ceth2
	nmcli con delete team0
	nmcli con reload
}

libteam_test_teamd()
{
	local runner=$1

	ip link set ceth1 down
	ip link set ceth2 down
	teamd -d -f $runner.json
	teamdctl team0 port add ceth1
	teamdctl team0 port add ceth2

	ip link set team0 up
	ip addr add 192.168.11.2/24 dev team0

	timeout 60s bash -c "until ping -c 3 192.168.11.1 >> libteam.log; do sleep 5; done" || \
	{ let LIBTEAM_FAIL++; libteam_log "FAIL $LIBTEAM_FAIL: teamd $runner testing failed"; }

	teamd -k -t team0
}

libteam_tests()
{
	local type=$1

	libteam_log "testing run $type =>"
	for runner in broadcast roundrobin activebackup loadbalance lacp; do
		libteam_log "runnner $runner testing start"
		libteam_setup_switch $runner
		eval "libteam_test_$type $runner"
		libteam_clean_switch $runner
		libteam_log "runnner $runner testing done\n"
	done
}

libteam_clean()
{
	ip link del ceth1
	ip link del ceth2
	ip link del br_seth1
	ip netn del serv1

	ovs-vsctl del-br br0
	libteam_stop_ovs
	libteam_clean_files
	libteam_log "testing clean\n\n"
}

LIBTEAM_FAIL=0
libteam_setup
libteam_tests nm
libteam_tests teamd
libteam_clean
exit $LIBTEAM_FAIL
